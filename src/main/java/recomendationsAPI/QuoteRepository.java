package recomendationsAPI;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Repositorio de simbolos para recuperar / guardar datos desde una coleccion mongo
 * @author terru
 *
 */
public interface QuoteRepository extends MongoRepository<Quote, String> {
	
	public Quote findByCode(String Code);
	public List<Quote> findByPrice(BigDecimal price);
}
