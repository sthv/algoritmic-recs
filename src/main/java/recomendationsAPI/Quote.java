package recomendationsAPI;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.annotation.Id;

/**
 * Representa una coleccion de simbolos que puede ser recuperable desde la API
 * REST
 * 
 * @url /api/quotes
 * @author terru
 * 
 */
public class Quote {

	@Id
	public String id;
	BigDecimal price;
	BigDecimal change;
	BigDecimal peg;
	BigDecimal dividend;
	String code;
	ArrayList<QuoteHistory> history;

	public Quote() {
		// puedo generar un quote vacio para despues setear los datos;
		this.history = new ArrayList<QuoteHistory>();
	}

	public Quote(String code, BigDecimal price, BigDecimal change,
			BigDecimal peg, BigDecimal dividend) {
		this.code = code;
		this.price = price;
		this.change = change;
		this.peg = peg;
		this.dividend = dividend;
		this.history = new ArrayList<QuoteHistory>();
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getChange() {
		return change;
	}

	public void setChange(BigDecimal change) {
		this.change = change;
	}

	public BigDecimal getPeg() {
		return peg;
	}

	public void setPeg(BigDecimal peg) {
		this.peg = peg;
	}

	public BigDecimal getDividend() {
		return dividend;
	}

	public void setDividend(BigDecimal dividend) {
		this.dividend = dividend;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void addToHistory(Date date, BigDecimal low, BigDecimal high,
			BigDecimal open, BigDecimal close) {
		QuoteHistory history = new QuoteHistory(date);
		history.setLow(low);
		history.setHigh(high);
		history.setOpen(open);
		history.setClose(close);
		this.history.add(history);
	}
	
	public ArrayList<QuoteHistory> getHistory() {
		return this.history;
	}

	
	
	// Representa el historico de un simbolo determinado
	private class QuoteHistory {

		private Date date; // la fecha del historial a representar
		private BigDecimal low, high, open, close;

		public QuoteHistory(Date date) {
			this.date = date;
		}

		public BigDecimal getLow() {
			return low;
		}

		public void setLow(BigDecimal low) {
			this.low = low;
		}

		public BigDecimal getHigh() {
			return high;
		}

		public void setHigh(BigDecimal high) {
			this.high = high;
		}

		public BigDecimal getOpen() {
			return open;
		}

		public void setOpen(BigDecimal open) {
			this.open = open;
		}

		public BigDecimal getClose() {
			return close;
		}

		public void setClose(BigDecimal close) {
			this.close = close;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

	}

}
