package recomendationsAPI;

import infoWrappers.AbstractWrapper;
import infoWrappers.YahooWrapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;

@Controller
public class QuoteController {

	@Autowired
	private QuoteRepository repository;
	// FIXME debería seleccionarse el wrapper desde una configuracion;
	private AbstractWrapper infoSource = new YahooWrapper();

	/** Recupera la informacion de un simbolo determinado desde la base de datos
	 * @param code
	 *            el nombre del simbolo que se requiere
	 * @return Quote {} datos de simbolo en base
	 */
	@GetMapping("/quotes")
	@ResponseBody
	public Quote getQuotes(
			@RequestParam(name = "code", required = false) String code) {
		System.out.println("Recuperando symbolo con codigo: " + code
				+ " desde la base de datos...");
		// se setea en un returner por si en un futuro hay que agregar un
		// wrapper o decorator a la respuesta http
		Quote returner = repository.findByCode(code);
		return returner;
	}

	/**
	 * Trae la informacion para un simbolo determinado desde la api financiera
	 * configurada y la guarda en la base
	 * 
	 * @param code
	 *            *required el codigo del simbolo a guardar
	 * @param date_from
	 *            la fecha desde cuando guardar el historial del simbolo (si no
	 *            viene se toman 5 años atras)
	 * @param date_to
	 *            la fecha hasta cuando guardar el historial (si no viene asume
	 *            la fecha actual)
	 * @return
	 */
	@PostMapping("/quotes")
	@ResponseBody
	public void setQuotes(
			@RequestParam(name = "code", required = true) String code,
			@RequestParam(name = "date_from", required = false) Date date_from,
			@RequestParam(name = "date_to", required = false) Date date_to) {
		// genero la query para la API correspondiente;

		// validacion de parametros no obligatorios en api rest
		if (date_from == null) {
			Calendar cal = Calendar.getInstance();
			// si no se especifica se toman 5 años por defecto
			cal.add(Calendar.YEAR, -5);
			date_from = cal.getTime(); // recupero un objeto tipo fecha que es
										// lo que espera el wrapper
		}
		if (date_to == null) {
			// si no se especifica se toma la fecha actual
			Calendar cal = Calendar.getInstance();
			date_to = cal.getTime(); // recupero un objeto tipo fecha que es lo
										// que espera el wrapper
		}
		// extraigo la informacion de la fuente de informacion
		System.out.println("Buscando la informacion del simbolo con codigo: "
				+ code);
		Quote new_quote = infoSource.getQuoteData(code, date_from, date_to);
		// guardar la informacion como formato Quote en la base
		System.out.println("Guardando la informacion obtenida.... ");
		repository.save(new_quote);
		System.out.println("Informacion guardada exitosamente");
	}

	/** Recupera la informacion de varios simbolos desde la base de datos
	 * @param String[]
	 *            <String> codes los simbolos a recuperar desde la base
	 * @return ArrayList<Quote> {} datos de simbolo en base
	 */
	@GetMapping("/quotes_all")
	@ResponseBody
	public ArrayList<Quote> getAllQuotes(
			@RequestParam(name = "codes", required = true) String[] codes) {
		// se setea en un returner por si en un futuro hay que agregar un
		// wrapper o decorator a la respuesta http
		ArrayList<Quote> returner = new ArrayList<Quote>();
		for (int i = 0; i < codes.length; i++) {
			System.out.println("Recuperando symbolo con codigo: " + codes[i]
					+ " desde la base de datos...");
			Quote to_add = repository.findByCode(codes[i]);
			returner.add(to_add);
		}
		return returner;
	}
	
	/**
	 * Trae informacion para varios simbolos desde una sourde de informacion determinada y la guarda en la base
	 * 
	 * @param String[] codes
	 *            *required el codigo de los simbolos a guardar
	 * @param date_from
	 *            la fecha desde cuando guardar el historial del simbolo (si no
	 *            viene se toman 5 años atras)
	 * @param date_to
	 *            la fecha hasta cuando guardar el historial (si no viene asume
	 *            la fecha actual)
	 * @return
	 */
	@PostMapping("/quotes_all")
	@ResponseBody
	public void setAllQuotes(
			@RequestParam(name = "codes", required = true) String[] codes,
			@RequestParam(name = "date_from", required = false) Date date_from,
			@RequestParam(name = "date_to", required = false) Date date_to) {
		// genero la query para la API correspondiente;

		// validacion de parametros no obligatorios en api rest
		if (date_from == null) {
			Calendar cal = Calendar.getInstance();
			// si no se especifica se toman 5 años por defecto
			cal.add(Calendar.YEAR, -5);
			date_from = cal.getTime(); // recupero un objeto tipo fecha que es
										// lo que espera el wrapper
		}
		if (date_to == null) {
			// si no se especifica se toma la fecha actual
			Calendar cal = Calendar.getInstance();
			date_to = cal.getTime(); // recupero un objeto tipo fecha que es lo
										// que espera el wrapper
		}
		// extraigo la informacion de la fuente de informacion
		System.out.println("Buscando la informacion de los simbolos: " + codes);
		
		ArrayList<Quote> new_quotes = infoSource.getQuotesData(codes, date_from, date_to);
		// guardar la informacion como formato Quote en la base
		System.out.println("Guardando la informacion obtenida.... ");
		repository.save(new_quotes);
		System.out.println("Informacion guardada exitosamente");
	}

}