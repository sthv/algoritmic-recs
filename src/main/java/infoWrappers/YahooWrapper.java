package infoWrappers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import recomendationsAPI.Quote;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

public class YahooWrapper extends AbstractWrapper {

	@Override
	protected Quote parseQuote(Object object_to_parse) {
		Quote parsed = new Quote();
		// downcast desde Object para adecuarlo al tipo de objeto que se parsea
		// en este wrapper
		Stock to_parse = (Stock) object_to_parse;
		// hago el mapping de datos
		parsed.setCode(to_parse.getSymbol());
		parsed.setPrice(to_parse.getQuote().getPrice());
		parsed.setChange(to_parse.getQuote().getChange());
		parsed.setPeg(to_parse.getStats().getPeg());
		parsed.setDividend(to_parse.getDividend().getAnnualYieldPercent());
		// parseo tambien el historial del simbolo
		try {
			List<HistoricalQuote> history = to_parse.getHistory();
			for (HistoricalQuote his : history) {
				parsed.addToHistory(his.getDate().getTime(), his.getLow(),
						his.getHigh(), his.getOpen(), his.getClose());
			}
			return parsed;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Quote getQuoteData(String quote, Date date_from, Date date_to) {
		// adapto los parametros del wrapper para yahoo
		Calendar dfrom = Calendar.getInstance();
		dfrom.setTime(date_from);
		Calendar dto = Calendar.getInstance();
		dto.setTime(date_to);
		// defino un parametro de consulta;
		Stock stock_info;
		// recupero el stock utilizando yahoo;
		// trato aqui mismo la exepcion porque es propia de yahoo, no de la
		// clase abstracta
		try {
			stock_info = YahooFinance.get(quote, dfrom, dto, Interval.WEEKLY);
			//debugging
			System.out.println("Adding quote: ");
			stock_info.print();
			// se utiliza un parser que debe implementarse en cada subclase
			// concreta para unificar la salida
			return this.parseQuote(stock_info);
		} catch (IOException e) {
			// detecto la exepcion y logeo el error;
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public ArrayList<Quote> getQuotesData(String[] quotes, Date date_from,
			Date date_to) {
		// busco primero todos los datos en yahoo y adapto los parametros
		ArrayList<Quote> returner = new ArrayList<Quote>();
		Calendar dfrom = Calendar.getInstance();
		dfrom.setTime(date_from);
		Calendar dto = Calendar.getInstance();
		dto.setTime(date_to);
		try {
			Map<String, Stock> stocks = YahooFinance.get(quotes, dfrom, dto,
					Interval.WEEKLY);
			// con todos los datos armo el arreglo que se espera que este metodo
			// devuelva;
			for (int i = 0; i < quotes.length; i++) {
				// debugging
				System.out.println("Adding quote: ");
				stocks.get(quotes[i]).print();
				// agrego al returner la informacion del simbolo
				Quote to_add = this.parseQuote(stocks.get(quotes[i]));
				returner.add(to_add);
			}
			return returner;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
