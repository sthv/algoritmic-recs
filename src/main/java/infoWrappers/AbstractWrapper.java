package infoWrappers;

import java.util.ArrayList;
import java.util.Date;

import recomendationsAPI.Quote;


public abstract class AbstractWrapper {
	public abstract Quote getQuoteData(String quote, Date date_from, Date date_to);
	public abstract ArrayList<Quote> getQuotesData(String[] quotes,Date date_from, Date date_to);
	protected abstract Quote parseQuote(Object object_to_parse);
}